<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePfTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'pf_types',
            function (Blueprint $table) {
                $table->Increments('pf_type_id')->comment('PK');
                $table->string('pf_type_name',10)->comment('PF類型名');
                $table->string('icon',20)->nullable()->comment('icon');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pf_types');
    }
}
