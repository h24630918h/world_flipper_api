<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBuildsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'builds',
            function (Blueprint $table) {
                $table->Increments('build_id')->comment('PK');

                $table->unsignedInteger('main1_id')->comment('main位1');
                $table->unsignedInteger('main2_id')->comment('main位2');
                $table->unsignedInteger('main3_id')->comment('main位3');

                $table->unsignedInteger('sub1_id')->comment('sub位1');
                $table->unsignedInteger('sub2_id')->comment('sub位2');
                $table->unsignedInteger('sub3_id')->comment('sub位3');

                $table->unsignedInteger('soul1_id')->comment('魂珠1');
                $table->unsignedInteger('soul2_id')->comment('魂珠2');
                $table->unsignedInteger('soul3_id')->comment('魂珠3');

                $table->string('analysis', 500)->comment('隊伍分析');
                $table->timestamps();


                $table->foreign('main1_id')->references('character_id')->on('characters')->onDelete('cascade');
                $table->foreign('main2_id')->references('character_id')->on('characters')->onDelete('cascade');
                $table->foreign('main3_id')->references('character_id')->on('characters')->onDelete('cascade');

                $table->foreign('sub1_id')->references('character_id')->on('characters')->onDelete('cascade');
                $table->foreign('sub2_id')->references('character_id')->on('characters')->onDelete('cascade');
                $table->foreign('sub3_id')->references('character_id')->on('characters')->onDelete('cascade');

                $table->foreign('soul1_id')->references('item_id')->on('items')->onDelete('cascade');
                $table->foreign('soul2_id')->references('item_id')->on('items')->onDelete('cascade');
                $table->foreign('soul3_id')->references('item_id')->on('items')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('builds');
    }
}
