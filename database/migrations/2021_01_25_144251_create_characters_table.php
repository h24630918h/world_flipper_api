<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'characters',
            function (Blueprint $table) {
                $table->Increments('character_id')->comment('PK');
                $table->string('character_name_zh', 10)->comment('角色名(中文)');
                $table->string('character_name_jp', 10)->comment('角色名(日文)');
                $table->tinyInteger('rare')->comment('稀有度');
                $table->string('title', 20)->comment('稱號');
                $table->tinyInteger('gender')->comment('性別');
                $table->string('get_way', 20)->comment('取得方式');
                $table->Integer('hp')->comment('血量');
                $table->Integer('atk')->comment('攻擊');
                $table->unsignedInteger('pf_type_id')->comment('power_flip類型');
                $table->unsignedInteger('type_id')->comment('屬性');
                $table->string('cv', 10)->comment('聲優');
                $table->string('story', 100)->comment('角色故事');

                $table->string('leader_skill_name', 10)->comment('隊長技(名稱)');
                $table->string('leader_skill', 100)->comment('隊長技');

                $table->string('ub_name', 10)->comment('奧義(名稱)');
                $table->string('ub', 100)->comment('奧義');
                $table->Integer('charge_bar')->comment('能量條');
                $table->string('ability1', 200)->comment('能力1');
                $table->string('ability2', 200)->comment('能力2');
                $table->string('ability3', 200)->comment('能力3');
                $table->string('ability4', 200)->comment('能力4');
                $table->string('ability5', 200)->comment('能力5');
                $table->string('ability6', 200)->comment('能力6');

                $table->string('character_image1', 20)->nullable()->comment('角色圖片進化前');
                $table->string('character_image2', 20)->nullable()->comment('角色圖片(進化後)');
                $table->timestamps();

                $table->foreign('pf_type_id')->references('pf_type_id')->on('pf_types')->onDelete('cascade');
                $table->foreign('type_id')->references('type_id')->on('types')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }
}
