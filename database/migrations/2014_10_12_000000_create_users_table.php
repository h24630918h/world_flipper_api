<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //status: 1.啟用,2.停用
        //role:   1.管理者,2.一般使用者
        //box_public: 1開放2:不開放
        Schema::create(
            'users',
            function (Blueprint $table) {
                $table->Increments('user_id')->comment('PK');
                $table->string('account', 12)->unique()->comment('帳號');
                $table->string('password', 60)->comment('密碼');
                $table->string('name', 20)->comment('姓名');
                $table->string('email', 50)->unique()->comment('信箱');
                $table->unsignedTinyInteger('box_public')->comment('是否開放個人角色盒子');
                $table->unsignedTinyInteger('status')->comment('帳號狀態');
                $table->unsignedTinyInteger('role')->comment('角色');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
