<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'user_boxes',
            function (Blueprint $table) {
                $table->Increments('user_box_id')->comment('PK');
                $table->unsignedInteger('character_id')->comment('角色id');
                $table->unsignedInteger('user_id')->comment('使用者id');
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_boxes');
    }
}
