<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'items',
            function (Blueprint $table) {
                $table->Increments('item_id')->comment('PK');
                $table->string('item_name_zh', 20)->comment('裝備名(中文)');
                $table->string('item_name_jp', 20)->comment('裝備名(日文)');
                $table->tinyInteger('rare')->comment('稀有度');
                $table->unsignedInteger('type_id')->comment('屬性');
                $table->Integer('hp')->comment('血量');
                $table->Integer('atk')->comment('攻擊');
                $table->string('ability', 200)->comment('裝備能力');
                $table->string('soul_ability', 200)->comment('魂珠能力');
                $table->string('get_way', 20)->comment('取得方式');
                $table->string('item_image', 20)->nullable()->comment('裝備圖片');

                $table->timestamps();

                $table->foreign('type_id')->references('type_id')->on('types')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
