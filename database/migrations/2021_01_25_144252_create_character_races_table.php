<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharacterRacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'character_races',
            function (Blueprint $table) {
                $table->Increments('character_race_id')->comment('PK');
                $table->unsignedInteger('character_id')->comment('角色PK');
                $table->unsignedInteger('race_id')->comment('種族PK');
                $table->timestamps();

                $table->unique(['character_id', 'race_id']);

                $table->foreign('character_id')->references('character_id')->on('characters')->onDelete('cascade');
                $table->foreign('race_id')->references('race_id')->on('races')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_races');
    }
}
