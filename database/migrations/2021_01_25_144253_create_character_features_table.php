<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharacterFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'character_features',
            function (Blueprint $table) {
                $table->Increments('character_feature_id')->comment('PK');
                $table->unsignedInteger('feature_id')->comment('特性PK');
                $table->unsignedInteger('character_id')->comment('角色PK');
                $table->timestamps();

                $table->unique(['feature_id', 'character_id']);
                $table->foreign('character_id')->references('character_id')->on('characters')->onDelete('cascade');
                $table->foreign('feature_id')->references('feature_id')->on('features')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_features');
    }
}
