<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now()->toDateString();
        User::insert(
            [
                'account' => 'wf_api_admin',
                'password' => bcrypt('zxc123456'),
                'name' => "wf_api_admin",
                'email' => 'wf_api_admin@gmail.com',
                'box_public' => 2,
                'status' => 1,
                'role' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ]
        );
    }
}
