<?php

use Illuminate\Database\Seeder;
use App\Entities\Model\PfType;

class PfTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now()->toDateString();
        PfType::insert(
            [
                [
                    'pf_type_name' => '格鬪',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'pf_type_name' => '劍士',
                    'created_at' => $now,
                    'updated_at' => $now,
                ],
                [
                    'pf_type_name' => '射擊',
                    'created_at' => $now,
                    'updated_at' => $now,
                ],
                [
                    'pf_type_name' => '特殊',
                    'created_at' => $now,
                    'updated_at' => $now,
                ],
                [
                    'pf_type_name' => '輔助',
                    'created_at' => $now,
                    'updated_at' => $now,
                ],

            ]
        );
    }
}
