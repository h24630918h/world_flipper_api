<?php

use Illuminate\Database\Seeder;
use App\Entities\Model\Type;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now()->toDateString();
        Type::insert(
            [
                [
                    'type_name' => '火',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'type_name' => '風',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'type_name' => '雷',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'type_name' => '水',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'type_name' => '光',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'type_name' => '闇',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
            ]
        );
    }
}
