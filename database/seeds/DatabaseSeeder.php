<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UsersTableSeeder');
        $this->call('PfTypesTableSeeder');
        $this->call('RacesTableSeeder');
        $this->call('TypesTableSeeder');
        $this->call('FeatureTableSeeder');
    }
}
