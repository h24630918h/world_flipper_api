<?php

use Illuminate\Database\Seeder;
use App\Entities\Model\Feature;

class FeatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now()->toDateString();
        Feature::insert(
            [
                [
                    'feature_name' => '能力傷害',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '充能',
                    'created_at' => $now,
                    'updated_at' => $now
                ],

                [
                    'feature_name' => '加速',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '增強效果',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '護盾',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '強化彈射',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => 'fever',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '協力球',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '回復再生',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '保護',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '弱體化解除',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '弱體耐性',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '強化解除',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '未分類',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '棺材',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '毒',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '浮游',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '渾身',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '耐性降低',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '背水',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '自傷',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '貫通',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '追蹤',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '連擊',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '遲鈍化',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'feature_name' => '麻痺',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
            ]
        );
    }
}
