<?php

use Illuminate\Database\Seeder;
use App\Entities\Model\Race;

class RacesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now()->toDateString();
        Race::insert(
            [
                [
                    'race_name' => '人',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'race_name' => '精靈',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'race_name' => '魔',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'race_name' => '獸',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'race_name' => '機械',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'race_name' => '妖',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'race_name' => '龍',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'race_name' => '不死',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'race_name' => '水棲',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
                [
                    'race_name' => '植物',
                    'created_at' => $now,
                    'updated_at' => $now
                ],
            ]
        );
    }
}
