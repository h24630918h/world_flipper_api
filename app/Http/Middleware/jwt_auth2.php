<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class jwt_auth2 extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $this->auth->parseToken()->authenticate();
            $user = Auth::guard()->user();
            if ($user) {
                $request['user'] = $user;
            } else {
                unset($request['user']);
            }
        } catch (JWTException $exception) {
            unset($request['account']);
        }
        return $next($request);
    }
}
