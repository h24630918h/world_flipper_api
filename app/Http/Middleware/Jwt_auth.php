<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class Jwt_auth extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $responseData["code"] = 401001;
        try {
            $this->auth->parseToken()->authenticate();
            $user = Auth::guard()->user();
            if ($user) {
                $request['user'] = $user;
                return $next($request);
            }
            $responseData["error"] = trans('auth.failed');
            return response()->json($responseData, 401, [], JSON_UNESCAPED_UNICODE);
        } catch (JWTException $e) {
            $responseData["error"] = $e->getMessage();
            return response()->json($responseData, 401, [], JSON_UNESCAPED_UNICODE);
        }
    }
}
