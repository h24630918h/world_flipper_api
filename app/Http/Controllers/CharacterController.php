<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\CharacterService;
use Validator;

class CharacterController extends Controller
{
    public $characterService;


    public function __construct()
    {
        $this->characterService = new CharacterService();
    }

    //region 新增角色資料

    /**
     * 新增角色資料
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newCharacter(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'payment_id' => 'required|integer',
                'description' => 'required|max:200|string',
                'order_detail' => 'required|array',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }

        #region 陣列內容 驗證
        $odItemJsonArray = array();
        $validArrayRule = array(
            'item_id' => 'required|integer',
            'quantity' => 'required|integer',
        );
        //遍歷整個array 將資料json_decode並驗證，若成功則推入$odItemJsonArray
        foreach ($postData['order_detail'] as $odItem) {
            $odItemJson = json_decode($odItem, true);
            //若json解碼失敗，取出錯誤碼並換成中文錯誤訊息並傳出
            if (!$odItemJson) {
                $result = json_last_error();
                $resultStr = $this->getJson_last_error($result);
                return $this->responseWithJson(array('error' => $resultStr, 'code' => config('apiCode.validateFail')));
            }
            array_push($odItemJsonArray, $odItemJson);
        }

        $odValidator = Validator::make($odItemJson, $validArrayRule);
        if ($odValidator->fails()) {
            return $this->responseWithJson(array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail')));
        }
        #endregion

        return $this->responseWithJson($this->characterService->newCharacter($postData, $odItemJsonArray));
    }
    //endregion



}
