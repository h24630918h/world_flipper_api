<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use Validator;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public $userService;


    public function __construct()
    {
        $this->userService = new UserService();
    }

    #region 會員註冊

    /**
     * 會員註冊
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function register(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:8,12',
                    'regex:/^(([a-z]+[0-9]+)|([0-9]+[a-z]+))[a-z0-9]*$/i',
                    'string',
                    'unique:users'
                ],
                'password' => [
                    'required',
                    'between:8,12',
                    'regex:/^(([a-z]+[0-9]+)|([0-9]+[a-z]+))[a-z0-9]*$/i',
                    'string',
                    'confirmed'
                ],
                'name' => 'required|max:20|string',
                'email' => 'required|max:50|string|unique:users',
                'box_public' => 'required|integer|between:1,2',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->userService->register($postData));
    }
    #endregion

    # region 會員登入

    /**
     * 會員登入
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function login(Request $request)
    {
        $postData = $request->only('account', 'password');
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:8,12',
                    'regex:/^(([a-z]+[0-9]+)|([0-9]+[a-z]+))[a-z0-9]*$/i',
                    'string'
                ],
                'password' => [
                    'required',
                    'between:8,12',
                    'regex:/^(([a-z]+[0-9]+)|([0-9]+[a-z]+))[a-z0-9]*$/i',
                    'string',
                ]
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->userService->login($postData));
    }
    #endregion


    #region 修改密碼
    /**
     * 修改密碼
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editPassword(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'password' => [
                    'required',
                    'between:8,12',
                    'regex:/^(([a-z]+[0-9]+)|([0-9]+[a-z]+))[a-z0-9]*$/i',
                    'string',
                    'confirmed'
                ]
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }
        return $this->responseWithJson($this->userService->editPassword($postData));
    }
    #endregion

    #region 啟用/停用帳戶
    /**
     * 啟用/停用帳戶
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editUserStatus(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:8,12',
                    'regex:/^(([a-z]+[0-9]+)|([0-9]+[a-z]+))[a-z0-9]*$/i',
                    'string'
                ],
            ]
        );
        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }
        return $this->responseWithJson($this->userService->editUserStatus($postData));
    }
    #endregion

    #region 修改帳號
    /**
     * 修改帳號
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editUser(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'box_public' => 'integer|between:1,2',
                'name' => 'max:20|string',
                'email' => 'max:50|string|unique:users',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->userService->editUser($postData));
    }

    #endregion


    #region  刪除會員資料

    /**
     *   刪除會員資料
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteUser(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:8,12',
                    'regex:/^(([a-z]+[0-9]+)|([0-9]+[a-z]+))[a-z0-9]*$/i',
                    'string'
                ],
            ]
        );
        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }
        return $this->responseWithJson($this->userService->deleteUser($postData));
    }
    #endregion

    #region 取得全部會員
    /**
     * 取得全部會員
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllUser()
    {
        return $this->responseWithJson($this->userService->getAllUser());
    }
    #endregion

    #region 取得會員資料 by帳號
    /**
     * 取得會員資料 by帳號
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getUserByAccount(Request $request)
    {
        $postData = array("account" => $request->account);
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:8,12',
                    'regex:/^(([a-z]+[0-9]+)|([0-9]+[a-z]+))[a-z0-9]*$/i',
                    'string'
                ],
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->userService->getUserByAccount($postData));
    }
    #endregion

    #region 更新擁有角色
    /**
     *   更新擁有角色
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateUserBox(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'user_box' => 'required|array',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        #region 陣列內容 驗證
        $ubItemJsonArray = array();
        $validArrayRule = array(
            'character_id' => 'required|integer',
        );
        //遍歷整個array 將資料json_decode並驗證，若成功則推入$odItemJsonArray
        foreach ($postData['user_box'] as $ubItem) {
            $ubItemJson = json_decode($ubItem, true);
            //若json解碼失敗，取出錯誤碼並換成中文錯誤訊息並傳出
            if (!$ubItemJson) {
                $result = json_last_error();
                $resultStr = $this->getJson_last_error($result);
                return $this->responseWithJson(array('error' => $resultStr, 'code' => config('apiCode.validateFail')));
            }
            array_push($ubItemJsonArray, $ubItemJson);

            $odValidator = Validator::make($ubItemJson, $validArrayRule);
            if ($odValidator->fails()) {
                return $this->responseWithJson(
                    array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
                );
            }
        }


        #endregion

        return $this->responseWithJson($this->userService->updateUserBox($postData, $ubItemJsonArray));
    }
    #endregion


    #region 忘記密碼
    /**
     *   忘記密碼
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function forgetPassword(Request $request)
    {
        $postData = array("account" => $request->account);
        $objValidator = Validator::make(
            $postData,
            [
                'account' => [
                    'required',
                    'between:8,12',
                    'regex:/^(([a-z]+[0-9]+)|([0-9]+[a-z]+))[a-z0-9]*$/i',
                    'string'
                ],
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array("result" => 1, 'code' => config('apiCode.success'))
            );
        }

        return $this->responseWithJson($this->userService->getUserByAccount($postData));
    }
    #endregion
}


