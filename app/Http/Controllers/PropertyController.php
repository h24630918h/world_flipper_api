<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PropertyService;
use Validator;


class PropertyController extends Controller
{
    public $propertyService;


    public function __construct()
    {
        $this->propertyService = new PropertyService();
    }

    #region 新增PfType

    /**
     * 新增PfType
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newPfType(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'pf_type_name' => 'required|max:10|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->newPfType($postData));
    }
    #endregion

    #region 修改PfType

    /**
     * 修改PfType
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editPfType(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'pf_type_id' => 'required|integer',
                'pf_type_name' => 'required|max:10|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->editPfType($postData));
    }
    #endregion

    #region 刪除PfType

    /**
     * 刪除PfType
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deletePfType(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'pf_type_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->deletePfType($postData));
    }
    #endregion

    #region 取得全部PfType
    /**
     * 取得全部PfType
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllPfType()
    {
        return $this->responseWithJson($this->propertyService->getAllPfType());
    }
    #endregion


    #region 新增種族

    /**
     * 新增種族
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newRace(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'race_name' => 'required|max:10|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->newRace($postData));
    }
    #endregion

    #region 修改種族

    /**
     * 修改種族
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editRace(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'race_id' => 'required|integer',
                'race_name' => 'required|max:10|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->editRace($postData));
    }
    #endregion

    #region 刪除種族

    /**
     * 刪除種族
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteRace(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'race_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->deleteRace($postData));
    }
    #endregion

    #region 取得全部種族
    /**
     * 取得全部種族
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllRace()
    {
        return $this->responseWithJson($this->propertyService->getAllRace());
    }
    #endregion

    #region 新增屬性

    /**
     * 新增屬性
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newType(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'type_name' => 'required|max:10|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->newType($postData));
    }
    #endregion

    #region 修改屬性

    /**
     * 修改屬性
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editType(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'type_id' => 'required|integer',
                'type_name' => 'required|max:10|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->editType($postData));
    }
    #endregion

    #region 刪除屬性

    /**
     * 刪除屬性
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteType(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'type_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->deleteType($postData));
    }
    #endregion



    #region 取得全部屬性
    /**
     * 取得全部屬性
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllType()
    {
        return $this->responseWithJson($this->propertyService->getAllType());
    }
    #endregion

    #region 新增特性

    /**
     * 新增特性
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newFeature(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'feature_name' => 'required|max:10|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->newFeature($postData));
    }
    #endregion

    #region 修改特性

    /**
     * 修改特性
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editFeature(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'feature_id' => 'required|integer',
                'feature_name' => 'required|max:10|string',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->editFeature($postData));
    }
    #endregion

    #region 刪除特性

    /**
     * 刪除特性
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteFeature(Request $request)
    {
        $postData = $request->all();
        $objValidator = Validator::make(
            $postData,
            [
                'feature_id' => 'required|integer',
            ]
        );

        if ($objValidator->fails()) {
            return $this->responseWithJson(
                array('error' => $objValidator->errors(), 'code' => config('apiCode.validateFail'))
            );
        }

        return $this->responseWithJson($this->propertyService->deleteFeature($postData));
    }
    #endregion


    #region 取得全部特性
    /**
     * 取得全部屬性
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAllFeature()
    {
        return $this->responseWithJson($this->propertyService->getAllFeature());
    }
    #endregion


}
