<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * 統整要回傳前端的資料格式
     *
     * @param array $info
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function responseWithJson(array $info)
    {
        $param = $this->handleResponseWithJson($info);

        if (!in_array($param['statusCode'], [200, 201])) { // 非 200 的一律寫入到 log
            unset($param['response']['result']);
            $error = $info['error'] ?? '';
            $param['response']['error'] = $error;
        }
        if (isset($info['other']) && $info['other'] != '') {
            $param['response']['other'] = $info['other'];
        }
        return response()->json($param['response'], $param['statusCode'], [], JSON_UNESCAPED_UNICODE);
    }

    /**
     * 整理回傳的資訊
     *
     * @param array $parameters
     * @return array
     */
    private function handleResponseWithJson($parameters)
    {
        // 整理APICode
        $apiCode = ($parameters['code'] && strlen($parameters['code']) == 6) ? $parameters['code'] : config(
            'apiCode.notAPICode'
        );
        return [
            'statusCode' => substr($apiCode, 0, 3),
            'response' => ['result' => ($parameters['result'] ?? ''), 'code' => $apiCode],
        ];
    }


}
