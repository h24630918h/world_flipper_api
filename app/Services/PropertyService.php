<?php

namespace App\Services;

use App\Repositories\Repository\PfTypeRepository;
use App\Repositories\Repository\RaceRepository;
use App\Repositories\Repository\TypeRepository;
use App\Repositories\Repository\FeatureRepository;

class PropertyService
{

    private $pfTypeRepository;
    private $raceRepository;
    private $typeRepository;
    private $featureRepository;


    public function __construct()
    {
        $this->pfTypeRepository = new PfTypeRepository();
        $this->raceRepository = new RaceRepository();
        $this->typeRepository = new TypeRepository();
        $this->featureRepository = new FeatureRepository();
    }

    #region  新增PfType

    /**
     * 新增PfType
     *
     * @param array $postData
     * @return array
     */
    public function newPfType($postData)
    {
        try {
            $this->pfTypeRepository->create($postData);
            return array('result' => 1, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  修改PfType

    /**
     * 修改PfType
     *
     * @param array $postData
     * @return array
     */
    public function editPfType($postData)
    {
        try {
            $pfTypeData = $this->pfTypeRepository->getPfTypeById($postData['pf_type_id']);
            if ($pfTypeData) {
                $pfTypeData->pf_type_name = $postData['pf_type_name'];
                $pfTypeData->save();
                return array('result' => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無資料", 'code' => config('apiCode.notFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  刪除PfType

    /**
     * 刪除PfType
     *
     * @param array $postData
     * @return array
     */
    public function deletePfType($postData)
    {
        try {
            $pfTypeData = $this->pfTypeRepository->getPfTypeById($postData['pf_type_id']);
            if ($pfTypeData) {
                $pfTypeData->delete();
                return array('result' => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無資料", 'code' => config('apiCode.notFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  取得全部PfType
    /**
     *  取得全部PfType
     *
     * @return array
     */
    public function getAllPfType()
    {
        try {
            $getPfTypeData = $this->pfTypeRepository->getAllPfType();
            return array("result" => $getPfTypeData, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion


    #region  新增種族

    /**
     *  新增種族
     *
     * @param array $postData
     * @return array
     */
    public function newRace($postData)
    {
        try {
            $this->raceRepository->create($postData);
            return array('result' => 1, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  修改種族

    /**
     * 修改種族
     *
     * @param array $postData
     * @return array
     */
    public function editRace($postData)
    {
        try {
            $raceData = $this->raceRepository->getRaceById($postData['race_id']);
            if ($raceData) {
                $raceData->race_name = $postData['race_name'];
                $raceData->save();
                return array('result' => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無資料", 'code' => config('apiCode.notFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  刪除種族

    /**
     * 刪除種族
     *
     * @param array $postData
     * @return array
     */
    public function deleteRace($postData)
    {
        try {
            $raceData = $this->raceRepository->getRaceById($postData['race_id']);
            if ($raceData) {
                $raceData->delete();
                return array('result' => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無資料", 'code' => config('apiCode.notFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  取得全部種族
    /**
     *  取得全部種族
     *
     * @return array
     */
    public function getAllRace()
    {
        try {
            $getRaceData = $this->raceRepository->getAllRace();
            return array("result" => $getRaceData, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  新增屬性

    /**
     *  新增屬性
     *
     * @param array $postData
     * @return array
     */
    public function newType($postData)
    {
        try {
            $this->typeRepository->create($postData);
            return array('result' => 1, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  修改屬性

    /**
     * 修改屬性
     *
     * @param array $postData
     * @return array
     */
    public function editType($postData)
    {
        try {
            $typeData = $this->typeRepository->getTypeById($postData['type_id']);
            if ($typeData) {
                $typeData->type_name = $postData['type_name'];
                $typeData->save();
                return array('result' => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無資料", 'code' => config('apiCode.notFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  刪除屬性

    /**
     * 刪除屬性
     *
     * @param array $postData
     * @return array
     */
    public function deleteType($postData)
    {
        try {
            $typeData = $this->typeRepository->getTypeById($postData['type_id']);
            if ($typeData) {
                $typeData->delete();
                return array('result' => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無資料", 'code' => config('apiCode.notFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  取得全部屬性
    /**
     *  取得全部屬性
     *
     * @return array
     */
    public function getAllType()
    {
        try {
            $getTypeData = $this->typeRepository->getAllType();
            return array("result" => $getTypeData, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  新增特性

    /**
     *  新增特性
     *
     * @param array $postData
     * @return array
     */
    public function newFeature($postData)
    {
        try {
            $this->featureRepository->create($postData);
            return array('result' => 1, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region 修改特性

    /**
     * 修改特性
     *
     * @param array $postData
     * @return array
     */
    public function editFeature($postData)
    {
        try {
            $featureData = $this->featureRepository->getTypeById($postData['feature_id']);
            if ($featureData) {
                $featureData->feature_name = $postData['feature_name'];
                $featureData->save();
                return array('result' => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無資料", 'code' => config('apiCode.notFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region 刪除特性

    /**
     * 刪除特性
     *
     * @param array $postData
     * @return array
     */
    public function deleteFeature($postData)
    {
        try {
            $featureData = $this->featureRepository->getTypeById($postData['feature_id']);
            if ($featureData) {
                $featureData->delete();
                return array('result' => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無資料", 'code' => config('apiCode.notFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion


    #region 取得全部特性

    /**
     * 取得全部特性
     *
     * @return array
     */
    public function getAllFeature()
    {
        try {
            $getFeatureData = $this->featureRepository->getAllFeature();
            return array('result' => $getFeatureData, 'code' => config('apiCode.success'));

        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

}
