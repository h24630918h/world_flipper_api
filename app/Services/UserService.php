<?php


namespace App\Services;


use App\Repositories\Repository\UserRepository;
use App\Repositories\Repository\UserBOXRepository;
use Tymon\JWTAuth\Facades\JWTAuth;
use Hash;
use Auth;

class UserService
{

    private $userRepository;
    private $userBOXRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
        $this->userBOXRepository = new  UserBOXRepository();
    }

    #region  會員註冊

    /**
     * 會員註冊
     *
     * @param array $postData
     * @return array
     */
    public function register($postData)
    {
        try {
            $postData['password'] = bcrypt($postData['password']);
            $postData['status'] = 1;
            $postData['role'] = 2;

            $newUser = $this->userRepository->create($postData);
            return array(
                'result' => array("token" => JWTAuth::fromUser($newUser), "user" => $newUser),
                'code' => config('apiCode.success')
            );
        } catch (JWTException $e) {
            return array('error' => $e->getMessage(), 'code' => $e->getCode() ?? config('apiCode.jwtExceptionCatch'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  會員登入
    /**
     * 會員登入
     *
     * @param array $postData
     * @return array
     */
    public function login($postData)
    {
        try {
            $userData = $this->userRepository->getUserByAccount($postData['account']);

            if ($userData) {
                if ($userData->status == 2) {
                    return array('error' => "帳號停用中", 'code' => config('apiCode.invalidPermission'));
                }

                if (Hash::check($postData['password'], $userData->password)) {
                    return array(
                        'result' => array("token" => JWTAuth::fromUser($userData), "user" => $userData,),
                        'code' => config('apiCode.success')
                    );
                }
                return array('error' => "密碼錯誤", 'code' => config('apiCode.invalidCredentials'));
            }
            return array('error' => "查無此會員", 'code' => config('apiCode.memberNotFound'));
        } catch (JWTException $e) {
            return array('error' => $e->getMessage(), 'code' => $e->getCode() ?? config('apiCode.jwtExceptionCatch'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion


    #region 修改密碼
    /**
     * 修改密碼
     *
     * @param array $postData
     * @return array
     */
    public function editPassword($postData)
    {
        try {
            $userData = $postData['user'];

            if ($userData) {
                $userData->password = bcrypt($postData['password']);
                $userData->save();
                return array("result" => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無此會員", 'code' => config('apiCode.memberNotFound'));
        } catch (JWTException $e) {
            return array('error' => $e->getMessage(), 'code' => $e->getCode() ?? config('apiCode.jwtExceptionCatch'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region 啟用/停用帳戶
    /**
     * 啟用/停用帳戶
     *
     * @param array $postData
     * @return array
     */
    public function editUserStatus($postData)
    {
        try {
            if ($postData['user']['role'] != 1) {
                return array("error" => '您不是管理員無法執行該功能', 'code' => config('apiCode.invalidPermission'));
            }

            $userData = $this->userRepository->getUserByAccount($postData['account']);

            if ($userData) {
                $userData->status = ($userData->status == 1) ? 2 : 1;
                $userData->save();
                return array("result" => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無此會員", 'code' => config('apiCode.memberNotFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }

    #endregion

    #region 修改使用者
    /**
     * 修改使用者
     *
     * @param array $postData
     * @return array
     */

    public function editUser($postData)
    {
        try {
            $userData = $postData['user'];

            if ($userData) {
                $userData->name = $postData['name'] ?? $userData->name;
                $userData->email = $postData['email'] ?? $userData->email;
                $userData->box_public = $postData['box_public'] ?? $userData->box_public;
                $userData->save();
                return array("result" => 1, 'code' => config('apiCode.success'));
            }
            return array('error' => "查無此會員", 'code' => config('apiCode.memberNotFound'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }

    #endregion

    #region 刪除會員資料
    /**
     * 刪除會員資料
     *
     * @param array $postData
     * @return array
     */
    public function deleteUser($postData)
    {
        try {
            if ($postData['user']['role'] !== 1) {
                return array("error" => '您不是管理員無法執行該功能', 'code' => config('apiCode.invalidPermission'));
            }

            $deleteData = $this->userRepository->getUserByAccount($postData['account']);

            if (!$deleteData) {
                return array("error" => '查無此會員', 'code' => config('apiCode.memberNotFound'));
            }

            $deleteData->delete();
            return array("result" => 1, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region 取得全部會員
    /**
     * 取得全部會員
     *
     * @return array
     */
    public function getAllUser()
    {
        try {
            $getUserData = $this->userRepository->getAllUser();
            return array("result" => $getUserData, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region  取得會員資料 by帳號
    /**
     * 取得會員資料 by帳號
     *
     * @param array $postData
     * @return array
     */
    public function getUserByAccount($postData)
    {
        try {
            $getUserData = $this->userRepository->getUserByAccount($postData['account']);
            return array("result" => $getUserData, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion

    #region 新增擁有角色
    /**
     *   新增擁有角色
     *
     * @param array $postData
     * @return array
     */
    public function updateUserBox($postData, $ubItemJsonArray)
    {
        try {
            DB::transaction(
                function () use ($postData, $ubItemJsonArray) {
                    foreach ($ubItemJsonArray as $item) {
                        $where = [
                            'user_id' => $postData['user']['user_id'],
                            'character_id' => $item['character_id'],
                        ];
                        $this->userBOXRepository->firstOrCreate($where);
                    }
                }
            );

            return array("result" => 1, 'code' => config('apiCode.success'));
        } catch (Exception $e) {
            return array('error' => $e->getMessage(), 'code' => config('apiCode.exceptionCatch'));
        } catch (Throwable $t) {
            return array('error' => $t->getMessage(), 'code' => config('apiCode.throwableCatch'));
        }
    }
    #endregion


    #region 忘記密碼
    /**
     *   忘記密碼
     *
     * @param array $postData
     * @return array
     */

    #endregion


}

