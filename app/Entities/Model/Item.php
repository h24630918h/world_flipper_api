<?php

namespace App\Entities\Model;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = 'items';
    protected $primaryKey = 'item_id';
    protected $fillable = [
        'item_name_zh',
        'item_name_jp',
        'rare',
        'type_id',
        'hp',
        'atk',
        'ability',
        'soul_ability',
        'get_way',
        'item_image'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
