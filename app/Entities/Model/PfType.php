<?php

namespace App\Entities\Model;

use Illuminate\Database\Eloquent\Model;

class PfType extends Model
{
    protected $table = 'pf_types';
    protected $primaryKey = 'pf_type_id';
    protected $fillable = [
        'pf_type_name','icon'
    ];
    protected $hidden = [
        'delete_at'
    ];

}
