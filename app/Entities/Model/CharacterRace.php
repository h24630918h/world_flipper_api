<?php

namespace App\Entities\Model;

use Illuminate\Database\Eloquent\Model;

class CharacterRace extends Model
{
    protected $table = 'character_races';
    protected $primaryKey = 'character_race_id';
    protected $fillable = [
        'character_id',
        'race_id'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
