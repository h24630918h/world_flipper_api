<?php

namespace App\Entities\Model;

use Illuminate\Database\Eloquent\Model;

class Build extends Model
{
    protected $table = 'builds';
    protected $primaryKey = 'build_id';
    protected $fillable = [
        'main1_id',
        'main2_id',
        'main3_id',
        'sub1_id',
        'sub2_id',
        'sub3_id',
        'soul1_id',
        'soul2_id',
        'soul3_id',
        'analysis'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
