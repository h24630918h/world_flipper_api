<?php

namespace App\Entities\Model;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{
    protected $table = 'characters';
    protected $primaryKey = 'character_id';
    protected $fillable = [
        'character_name_zh',
        'character_name_jp',
        'rare',
        'title',
        'gender',
        'get_way',
        'hp',
        'atk',
        'pf_type_id',
        'type_id',
        'cv',
        'story',
        'leader_skill_name',
        'leader_skill',
        'ub_name',
        'ub',
        'charge_bar',
        'ability1',
        'ability2',
        'ability3',
        'ability4',
        'ability5',
        'ability6',
        'character_image1',
        'character_image2'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
