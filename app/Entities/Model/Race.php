<?php

namespace App\Entities\Model;

use Illuminate\Database\Eloquent\Model;

class Race extends Model
{
    protected $table = 'races';
    protected $primaryKey = 'race_id';
    protected $fillable = [
        'race_name','icon'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
