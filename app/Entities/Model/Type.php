<?php

namespace App\Entities\Model;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    protected $table = 'types';
    protected $primaryKey = 'type_id';
    protected $fillable = [
        'type_name','icon'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
