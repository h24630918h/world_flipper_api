<?php

namespace App\Entities\Model;

use Illuminate\Database\Eloquent\Model;

class UserBox extends Model
{
    protected $table = 'user_boxes';
    protected $primaryKey = 'user_box_id';
    protected $fillable = [
        'character_id',
        'user_id'
    ];
    protected $hidden = [
        'delete_at'
    ];
}
