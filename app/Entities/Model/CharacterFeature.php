<?php

namespace App\Entities\Model;

use Illuminate\Database\Eloquent\Model;

class CharacterFeature extends Model
{
    protected $table = 'character_features';
    protected $primaryKey = 'character_feature_id';
    protected $fillable = [
        'feature_id',
        'character_id',
    ];
    protected $hidden = [
        'delete_at'
    ];
}
