<?php

namespace App\Entities\Model;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = 'features';
    protected $primaryKey = 'feature_id';
    protected $fillable = [
        'feature_name',
    ];
    protected $hidden = [
        'delete_at'
    ];
}
