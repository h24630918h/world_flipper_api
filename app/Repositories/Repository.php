<?php

namespace App\Repositories;

use DB;

trait Repository
{
    private $entity;

    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    //通用語法

    /**
     * 新增資料()
     *
     * @param
     * @param array $field [新增資料所需的欄位資料]
     * @return mixed
     */

    public function create($field = ['*'])
    {
        return $this->entity::create($field);
    }


    /**
     * 資料新增，存在則更新
     *
     * @param array $where [條件]
     * @param array $parameters [更新資料參數]
     * @return mixed
     */

    public function firstOrCreate(array $where, array $parameters=[])
    {
        return $this->entity::firstOrCreate($where, $parameters);
    }


}
