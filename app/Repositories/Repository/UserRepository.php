<?php

namespace App\Repositories\Repository;

use DB;
use App\User;
use App\Repositories\Repository;

class UserRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(User::class);
    }


    /**
     * 取得會員資料
     *
     * @param string $account 帳號
     *
     * @return array
     */
    public function getUserByAccount($account)
    {
        return User::where('account', $account)->first();
    }


    /**
     * 取得全部會員
     *
     * @return array
     */
    public function getAllUser()
    {
        return User::orderByDesc('created_at')->get();
    }


}
