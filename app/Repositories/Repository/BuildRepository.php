<?php

namespace App\Repositories\Repository;

use DB;

use App\Entities\Model\Build;
use App\Repositories\Repository;

class BuildRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(Build::class);
    }
}
