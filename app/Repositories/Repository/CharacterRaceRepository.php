<?php

namespace App\Repositories\Repository;

use DB;

use App\Entities\Model\CharacterRace;
use App\Repositories\Repository;

class CharacterRaceRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(CharacterRace::class);
    }
}

