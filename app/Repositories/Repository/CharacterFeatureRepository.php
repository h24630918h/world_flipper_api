<?php

namespace App\Repositories\Repository;

use DB;

use App\Entities\Model\CharacterFeature;
use App\Repositories\Repository;

class CharacterFeatureRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(CharacterFeature::class);
    }
}
