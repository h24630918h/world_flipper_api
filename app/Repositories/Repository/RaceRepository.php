<?php

namespace App\Repositories\Repository;

use DB;

use App\Entities\Model\Race;
use App\Repositories\Repository;

class RaceRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(Race::class);
    }


    /**
     * 取得種族 by id
     *
     * @param string $id 帳號
     *
     * @return array
     */
    public function getRaceById($id)
    {
        return Race::where('race_id', $id)->first();
    }


    /**
     * 取得全部種族
     *
     * @return array
     */
    public function getAllRace()
    {
        return Race::orderByDesc('created_at')->get();
    }
}
