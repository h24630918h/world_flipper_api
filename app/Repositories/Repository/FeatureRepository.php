<?php

namespace App\Repositories\Repository;

use DB;

use App\Entities\Model\Feature;
use App\Repositories\Repository;

class FeatureRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(Feature::class);
    }


    /**
     * 取得特性 by id
     *
     * @param string $id 帳號
     *
     * @return array
     */
    public function getFeatureById($id)
    {
        return Feature::where('feature_id', $id)->first();
    }


    /**
     * 取得全部特性
     *
     * @return array
     */
    public function getAllFeature()
    {
        return Feature::orderByDesc('created_at')->get();
    }
}
