<?php

namespace App\Repositories\Repository;

use DB;

use App\Entities\Model\Item;
use App\Repositories\Repository;

class ItemRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(Item::class);
    }
}
