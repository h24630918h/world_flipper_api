<?php


namespace App\Repositories\Repository;

use DB;

use App\Entities\Model\PfType;
use App\Repositories\Repository;

class PfTypeRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(PfType::class);
    }

    /**
     * 取得pftype by id
     *
     * @param string $id 帳號
     *
     * @return array
     */
    public function getPfTypeById($id)
    {
        return PfType::where('pf_type_id', $id)->first();
    }


    /**
     * 取得全部pftype
     *
     * @return array
     */
    public function getAllPfType()
    {
        return PfType::orderByDesc('created_at')->get();
    }

}
