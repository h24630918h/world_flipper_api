<?php

namespace App\Repositories\Repository;

use DB;

use App\Entities\Model\Character;
use App\Repositories\Repository;

class CharacterRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(Character::class);
    }
}
