<?php

namespace App\Repositories\Repository;

use DB;

use App\Entities\Model\UserBox;
use App\Repositories\Repository;

class UserBoxRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(UserBox::class);
    }
}
