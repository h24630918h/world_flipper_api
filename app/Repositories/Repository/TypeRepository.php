<?php

namespace App\Repositories\Repository;

use DB;

use App\Entities\Model\Type;
use App\Repositories\Repository;

class TypeRepository
{
    use Repository;

    public function __construct()
    {
        $this->setEntity(Type::class);
    }

    /**
     * 取得戰鬥類型 by id
     *
     * @param string $id 帳號
     *
     * @return array
     */
    public function getTypeById($id)
    {
        return Type::where('type_id', $id)->first();
    }


    /**
     * 取得戰鬥類型
     *
     * @return array
     */
    public function getAllType()
    {
        return Type::orderByDesc('created_at')->get();
    }
}
