<?php

// api code

return [
    /**
     * HTTP STATUS CODE : ２００
     */
    // 執行成功
    'success' => 200001,

    /**
     * HTTP STATUS CODE    : ４００
     */

    /**
     * HTTP STATUS CODE    : ４０１
     */
    // 登入失敗
    'invalidCredentials' => 401001,
    // 抓到jwtexception
    'jwtExceptionCatch' => 404102,

    /**
     * HTTP STATUS CODE    : ４０３
     */
    // 無該功能權限
    'invalidPermission' => 403001,

    /**
     * HTTP STATUS CODE    : ４０４
     */
    // 查無資料
    'notFound' => 404001,
    // 查無此會員
    'memberNotFound' => 404101,


    /**
     * HTTP STATUS CODE    : ４１７
     */
    // 執行失敗
    'expectationFailed' => 417001,

    /**
     * HTTP STATUS CODE    : ４２２
     */
    // 驗證失敗
    'validateFail' => 422001,
    // 名稱重複
    'validateRepeat' => 422002,

    /**
     * HTTP STATUS CODE    : ５００
     */
    // 未傳遞 API Code
    'notAPICode' => 500001,
    // 產生 Token 異常
    'couldNotCreateToken' => 500002,
    // 抓到exception
    'exceptionCatch' => 500003,
    // 抓到exception 或 error
    'throwableCatch' => 500004,

    /**
     * HTTP STATUS CODE    : ５０3
     */
    'ServiceUnavailable' => 503001,
];
