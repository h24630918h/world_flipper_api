<?php

use Illuminate\Http\Request;

Route::group(
    ['middleware' => ['displayChinese', 'cors']],
    function () {
        //未登入功能
        Route::post('register', 'UserController@register');
        Route::post('login', 'UserController@login');
        Route::get('getAllPfType', 'PropertyController@getAllPfType');
        Route::get('getAllRace', 'PropertyController@getAllRace');
        Route::get('getAllType', 'PropertyController@getAllType');
        Route::get('getAllFeature', 'PropertyController@getAllFeature');

        Route::group(
            ['middleware' => 'jwt_auth'],
            function () {
                //user功能
                Route::put('editPassword', 'UserController@editPassword');
                Route::put('editUser', 'UserController@editUser');
                Route::delete('deleteUser', 'UserController@deleteUser');
                Route::put('editUserStatus', 'UserController@editUserStatus');

                //系統屬性參數功能 (屬性 pf 種族 特性)
                Route::post('newPfType', 'PropertyController@newPfType');
                Route::put('editPfType', 'PropertyController@editPfType');
                Route::delete('deletePfType', 'PropertyController@deletePfType');
                Route::post('newRace', 'PropertyController@newRace');
                Route::put('editRace', 'PropertyController@editRace');
                Route::delete('deleteRace', 'PropertyController@deleteRace');
                Route::post('newType', 'PropertyController@newType');
                Route::put('editType', 'PropertyController@editType');
                Route::delete('deleteType', 'PropertyController@deleteType');
                Route::post('newFeature', 'PropertyController@newFeature');
                Route::put('editFeature', 'PropertyController@editFeature');
                Route::delete('deleteFeature', 'PropertyController@deleteFeature');
            }
        );
        Route::get('getAllUser', 'UserController@getAllUser');
        Route::get('getUserByAccount', 'UserController@getUserByAccount');
    }
);
